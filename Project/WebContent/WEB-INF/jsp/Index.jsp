<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/original/login.css" rel="stylesheet">
</head>
<body>


	<!-- body -->


	<form class="form-signin" action="LoginServlet" method="post">
		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>
		<div class="text-center mb-4">
			<h1 class="h3 mb-3 font-weight-normal">ログイン画面</h1>
		</div>

		<div class="form-label-group row">
			<label class="col-sm-3" for="inputLoginId">ログインID</label>
			<div class="col-sm-9">
				<input type="text" name="loginId" class="form-control"
					placeholder="ログインIDを入力してください" autofocus>
			</div>
		</div>

		<div class="form-label-group row">
			<label class="col-sm-3" for="inputPassword">パスワード</label>
			<div class="col-sm-9">
				<input type="password" name="password" class="form-control"
					placeholder="パスワードを入力してください">
			</div>
		</div>


		<div class="text-center mb-4">

			<button class="btn btn-lg btn-primary btn-block" type="submit">ログイン</button>
		</div>


	</form>


</body>
</html>
