package model;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private Date birthDate;
	private Date birthDateStart;
	private Date birthDateEnd;
	private String password;
	private String passwordConf;
	private String createDate;
	private String updateDate;

	public User(String loginId) {
		this.loginId = loginId;

	}

	public User(String loginId, String name) {
		this.loginId = loginId;
		this.name = name;
	}

	public User(String loginId, String name, Date bithDateStart, Date birthDateEnd) {
		this.loginId = loginId;
		this.name = name;
		this.birthDateStart = bithDateStart;
		this.birthDateEnd = birthDateEnd;
	}

	public User(int id, String loginId, String name, Date birthDate, String password, String passwordConf,
			String createDate,
			String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.passwordConf = passwordConf;
		this.createDate = createDate;
		this.updateDate = updateDate;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConf() {
		return passwordConf;
	}

	public void setPasswordConf(String passwordConf) {
		this.passwordConf = passwordConf;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public Date getBirthDateStart() {
		return birthDateStart;
	}

	public void setBirthDateStart(Date birthDateStart) {
		this.birthDateStart = birthDateStart;
	}

	public Date getBirthDateEnd() {
		return birthDateEnd;
	}

	public void setBirthDateEnd(Date birthDateEnd) {
		this.birthDateEnd = birthDateEnd;
	}

}
